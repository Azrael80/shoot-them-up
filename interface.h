#ifndef INTERFACE_H
#define INTERFACE_H
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include "spaceShip.h"
#define WINDOW_WEIGHT 720
#define WINDOW_HEIGHT 520

void createGameInterface(); /* Créer l'interface de jeu. */
SDL_Texture* loadTextureFromImage(SDL_Renderer* renderer, char* image_path); /* Créer une texture à partir de l'emplacement d'une image */
void refreshRenderer(); /* Permet d'actualiser l'affichage de l'écran */
void startGameLoop(); /* Lancement de la boucle du jeu */
void getKeyboard(const unsigned char*); /* Gestion des touches clavier */
void updateLasers(); /* Mise à jour des lasers (déplacements, suppression, ennemi toucher...) */
void updateEnemies(); /* Mise à jour des ennemies (déplacements, suppression...) */
void updateExplosions(); /* Animation des explosions */
void spawnEnemyRandom(); /* Apparition aléatoire des ennemies */
void getLaserCollision(); /* Vérification des collisions entre laser ennemies */
void showScore(); /* Ajoute le score au renderer */

#endif

