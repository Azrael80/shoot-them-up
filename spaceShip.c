#include "spaceShip.h"

spaceShip createSpaceShip(SDL_Renderer* renderer)
{
	spaceShip ship;
	ship.rect.x = 335;
	ship.rect.y = WINDOW_HEIGHT - 50;
	ship.rect.w = 50;
	ship.rect.h = 50;
	ship.texture = loadTextureFromImage(renderer, "images/ship.png");
	ship.speed = 9;
	ship.nextMove = 0;
	ship.nextFire = 0;
	return ship;
}

void spaceShipFire(SDL_Renderer* renderer, spaceShipLaser* lasers[1000], spaceShip ship)
{
	spaceShipLaser* laser = (struct spaceShipLaser*)malloc(sizeof(struct spaceShipLaser));

	/* Placement du laser devant le vaisseau */

	laser->rect.x = ship.rect.x + 23;
	laser->rect.y = ship.rect.y - 25;
	laser->rect.w = 5;
	laser->rect.h = 25;
	laser->texture = loadTextureFromImage(renderer, "images/laser.png");
	for(int i = 0; i < 1000; i++) 
	{
		if(lasers[i] == NULL) 
		{
			lasers[i] = laser;
			break;
		}
	}
}

spaceShip* createEnemyShip(SDL_Renderer* renderer, int x, int y, int type, SDL_Texture* texture)
{
	spaceShip* ship = (struct spaceShip*)malloc(sizeof(struct spaceShip));
	ship->rect.x = x;
	ship->rect.y = y;
	ship->rect.w = 50;
	ship->rect.h = 50;
	ship->texture = texture;
	ship->speed = type == 1 ? 5 : type == 2 ? 7 : 3;
	ship->nextMove = 0;
	ship->nextFire = 0;
	ship->points = type == 1 ? 15 : type == 2 ? 25 : 10;
	return ship;
}

spaceShipExplosion* createExplosion(spaceShip* ship)
{
	spaceShipExplosion* explosion = (struct spaceShipExplosion*)malloc(sizeof(struct spaceShipExplosion));

	explosion->ship = ship;
	explosion->step = 0;
	explosion->nextStep = 0;

	return explosion;
}
