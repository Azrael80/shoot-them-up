#ifndef SPACESHIP_H
#define SPACESHIP_H
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <time.h>
#include "interface.h"

/* Structure vaisseau */
struct spaceShip {
	SDL_Texture* texture;
	SDL_Rect rect;
	int speed; /* vitesse du vaisseau */
	int nextMove; /* prochain déplacement possible (si = 0) */
	int nextFire; /* prochain tire possible (si = 0) */
	int points; /* nombre de points que donne la destruction */
};
typedef struct spaceShip spaceShip;

/* Structure laser */
struct spaceShipLaser {
	SDL_Texture* texture;
	SDL_Rect rect;
};
typedef struct spaceShipLaser spaceShipLaser;

/* Structure animation explosion */
struct spaceShipExplosion {
	struct spaceShip* ship;
	short int step; /* Pas sur l'animation d'explosion */
	unsigned int nextStep; /* Prochain passage de pas en millisecondes */
};
typedef struct spaceShipExplosion spaceShipExplosion;

spaceShip createSpaceShip(SDL_Renderer* renderer); /* Créer un vaisseau */
void spaceShipFire(SDL_Renderer* renderer, spaceShipLaser* lasers[1000], spaceShip ship); /* Faire tirer un vaisseau */
spaceShip* createEnemyShip(SDL_Renderer* renderer, int x, int y, int type, SDL_Texture* texture); /* Créer un ennemi */
spaceShipExplosion* createExplosion(spaceShip* ship); /* Créer une animation d'explosion a partir d'un vaisseau */

#endif
