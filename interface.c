#include <time.h>
#include "interface.h"
#include "spaceShip.h"

SDL_Window* window = NULL;  
SDL_Renderer* renderer = NULL;

/* Textures */
SDL_Texture* textureBackground = NULL;
SDL_Texture* scoreTexture = NULL;
SDL_Texture* shipTextures[3];
SDL_Texture* explosionTextures[7];


TTF_Font* font = NULL;
spaceShip ship;
spaceShipLaser* lasers[1000];
spaceShip* enemies[1000];
spaceShipExplosion* explosions[1000];
unsigned int futurSpawnEnemy = 0;
unsigned int timeStart = 0;
int score = 0;

void createGameInterface()
{
    SDL_Init(SDL_INIT_VIDEO);
    TTF_Init();

    font = TTF_OpenFont("polices/freshman.ttf", 50);

    window = SDL_CreateWindow(
        "Shoot them up (PROJET)",                  
        SDL_WINDOWPOS_UNDEFINED,           
        SDL_WINDOWPOS_UNDEFINED,           
        WINDOW_WEIGHT,                               
        WINDOW_HEIGHT,                               
        SDL_WINDOW_OPENGL                  
    );

    if (window == NULL) {
        printf("Could not create window: %s\n", SDL_GetError());
        return;
    }

	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);

	textureBackground = loadTextureFromImage(renderer, "./images/background.jpg"); /* Chargement de la texture image de fond */
	ship = createSpaceShip(renderer); /* Chargement du vaisseau du joueur */

	/* Chargement des textures des vaisseaux énnemies */
	shipTextures[0] = loadTextureFromImage(renderer, "./images/enemy_1.png");
	shipTextures[1] = loadTextureFromImage(renderer, "./images/enemy_2.png");
	shipTextures[2] = loadTextureFromImage(renderer, "./images/enemy_3.png");

	/* Chargement des textures d'explosion */
	explosionTextures[0] = loadTextureFromImage(renderer, "./images/exp_1.png");
	explosionTextures[1] = loadTextureFromImage(renderer, "./images/exp_2.png");
	explosionTextures[2] = loadTextureFromImage(renderer, "./images/exp_3.png");
	explosionTextures[3] = loadTextureFromImage(renderer, "./images/exp_4.png");
	explosionTextures[4] = loadTextureFromImage(renderer, "./images/exp_5.png");
	explosionTextures[5] = loadTextureFromImage(renderer, "./images/exp_6.png");
	explosionTextures[6] = loadTextureFromImage(renderer, "./images/exp_7.png");

    refreshRenderer(); /* Premier affichage */

	timeStart = time(NULL);

	startGameLoop(); /* Début de la boucle du jeu avec récupération des évènements */

	/* Fin du jeu */
	/* Destruction des textures */
	SDL_DestroyTexture(shipTextures[0]);
	SDL_DestroyTexture(shipTextures[1]);
	SDL_DestroyTexture(shipTextures[2]);
	SDL_DestroyTexture(explosionTextures[0]);
	SDL_DestroyTexture(explosionTextures[1]);
	SDL_DestroyTexture(explosionTextures[2]);
	SDL_DestroyTexture(explosionTextures[3]);
	SDL_DestroyTexture(explosionTextures[4]);
	SDL_DestroyTexture(explosionTextures[5]);
	SDL_DestroyTexture(explosionTextures[6]);
	SDL_DestroyTexture(textureBackground);
	SDL_DestroyTexture(scoreTexture);
	SDL_DestroyTexture(ship.texture);

	SDL_RenderClear(renderer);		
    SDL_DestroyWindow(window);

    TTF_CloseFont(font);
    TTF_Quit();
    SDL_Quit();
}

void refreshRenderer()
{
	SDL_RenderClear(renderer);
	SDL_RenderCopy(renderer, textureBackground, NULL, NULL);
	updateLasers(); /* Mise à jour des tirs */
	updateEnemies(); /* Mise à jour des ennemies */
	SDL_RenderCopy(renderer, ship.texture, NULL, &ship.rect);
	updateExplosions(); /* Mise à jour des explosions */
	showScore();
 	SDL_RenderPresent(renderer);
 	getLaserCollision(); /* Vérification des collisions pour le prochain affichage */
}

SDL_Texture* loadTextureFromImage(SDL_Renderer* renderer, char* image_path)
{
	SDL_Texture* texture = NULL;  
	SDL_Surface* image = NULL;   
	IMG_Init(IMG_INIT_JPG|IMG_INIT_PNG);
	image = IMG_Load(image_path);
	texture = SDL_CreateTextureFromSurface(renderer, image);
    SDL_FreeSurface(image);
	IMG_Quit();
	
	return texture;
}

void startGameLoop()
{
	SDL_Event event;
	char closeLoop = 0;
	const unsigned char* state =  NULL;
	int i = 0;

	/* Initialisation des tableaux sur NULL */
	for(i = 0; i < 1000; i++) 
	{ 
		lasers[i] = NULL;
		enemies[i] = NULL;
		explosions[i] = NULL;
	}

	while (closeLoop != 1)
    {
      	while (SDL_PollEvent(&event))
      	{
      		switch(event.type)
      		{
      			case SDL_QUIT: /* Le jeu doit se fermer, on stop la boucle */
      				closeLoop = 1;
      				break;
      		}
     	}

     	if(ship.nextMove > 0) ship.nextMove -= 10; /* Retire 10ms d'attente pour le prochain déplacement du vaisseau */

     	if(ship.nextFire > 0) ship.nextFire -= 10; /* Retire 10ms d'attente pour le prochain tire du vaisseau */

     	state = SDL_GetKeyboardState(NULL);
		getKeyboard(state); /* Gestion des touches du clavier */

		spawnEnemyRandom(); /* Gestion des ennemies */

		refreshRenderer(); /* Mise à jour de l'affichage */
		SDL_Delay(10); /* Attendre: 10ms */
    }
}

void getKeyboard(const unsigned char* keys)
{
	char move = 0;
	char fire = 0;

	/* Déplacements */

	if(ship.nextMove <= 0) /* On vérifie que le vaisseau peut bouger, sinon ça va trop vite ! */
	{
		/*
		/*	Mouvement haut/bas 
	    if(keys[SDL_SCANCODE_UP]) //Flèche du haut appuyée 
	    {
	        ship.rect.y -= ship.speed;
	        move = 1;
	    }
	    if(keys[SDL_SCANCODE_DOWN]) //Flèche du bas appuyée 
	    {
	        ship.rect.y += ship.speed;
	        move = 1;
	    }
		*/
		
	    if(keys[SDL_SCANCODE_LEFT]) /* Flèche de gauche appuyée */
	    {
	        ship.rect.x -= ship.speed;
	        move = 1;
	    }
	   	if(keys[SDL_SCANCODE_RIGHT]) /* Flèche de droite appuyée  */
	    {
	        ship.rect.x += ship.speed;
	        move = 1;
	    }
	}

	if(move == 1) 
    {
    	ship.nextMove = 30;
    	/* On vérifie que le vaisseau ne sort pas de la fenêtre. */
    	if(ship.rect.x < 0) ship.rect.x = 0;
    	if(ship.rect.x > WINDOW_WEIGHT - 50) ship.rect.x = WINDOW_WEIGHT - 50; /* -50 car largueur de l'image du vaisseau */
    	if(ship.rect.y < 0) ship.rect.y = 0;
    	if(ship.rect.y > WINDOW_HEIGHT - 50) ship.rect.y = WINDOW_HEIGHT - 50; /* -50 car hauteur de l'image du vaisseau */
    }

	/* Tires */

    if(ship.nextFire <= 0)
    {
		if(keys[SDL_SCANCODE_SPACE]) /* Touche espace appuyée : tire  */
		{
		    spaceShipFire(renderer, lasers, ship);
		    fire = 1;
		}
	}
    
    if(fire == 1)
    {
    	ship.nextFire = 250;
    }
}

void updateLasers()
{
	for(int i = 0; i < 1000; i++)
	{
		if(lasers[i] != NULL)
		{
			//Deplacement
			lasers[i]->rect.y -= 3;


			if((lasers[i]->rect.y + 30) <= 0) /* Le laser sort de l'écran, on le supprime */
			{
				SDL_DestroyTexture(lasers[i]->texture);
				free(lasers[i]);
				lasers[i] = NULL;
			}
			else
			{
				SDL_RenderCopy(renderer, lasers[i]->texture, NULL, &lasers[i]->rect);
			}
		}
	}
}

void updateEnemies()
{
	for(int i = 0; i < 1000; i++)
	{
		if(enemies[i] != NULL)
		{
			/* Déplacements */
			if(enemies[i]->nextMove > 0) enemies[i]->nextMove -= 10;
			if(enemies[i]->nextMove <= 0)
			{
				enemies[i]->rect.y += enemies[i]->speed;
				enemies[i]->nextMove = 30;
			}
			if(enemies[i]->rect.y > WINDOW_HEIGHT) /* L'ennemie sort de l'écran */
			{
				score -= (enemies[i]->points / 2); /* Retrait des points au score, l'ennemie est sortie */
				free(enemies[i]);
				enemies[i] = NULL;
			}
			else
			{
				SDL_RenderCopy(renderer, enemies[i]->texture, NULL, &enemies[i]->rect);
			}
		}
	}
}

void updateExplosions()
{
	for(int i = 0; i < 1000; i++)
	{
		if(explosions[i] != NULL)
		{
			/* Animation de l'explosion */
			if(explosions[i]->nextStep == 0) 
			{
				explosions[i]->step += 1;
				explosions[i]->nextStep = 50;
			}
			else
				explosions[i]->nextStep -= 10;

			if(explosions[i]->step == 7) /* Fin de l'animation, suppression du vaisseau et de l'explosion */
			{
				free(explosions[i]->ship);
				free(explosions[i]);
				explosions[i] = NULL;
			}
			else
			{
				if(explosions[i]->step < 4) /* Le vaisseau n'apparait plus après l'étape 3 de l'explosion */
					SDL_RenderCopy(renderer, explosions[i]->ship->texture, NULL, &explosions[i]->ship->rect);
				SDL_RenderCopy(renderer, explosionTextures[explosions[i]->step], NULL, &explosions[i]->ship->rect);
			}
		}
	}
}

void spawnEnemyRandom()
{
	unsigned int actualTime = time(NULL);
	int x = 0;
	int number = 1;
	int enemy_rand = 0;
	int type = 0;

	srand(time(NULL));

	number = 1 + rand() % 5; /* Nombre d'ennemies */

	if(actualTime >= futurSpawnEnemy)
	{
		for(int n = 0; n < number; n++)
		{
			x = ((rand() % (WINDOW_WEIGHT / 50)) * 50) + 10; /* Choix de la position X en fonction de la largeur de l'écran */
			for(int i = 0; i < 1000; i++)
			{
				enemy_rand = rand() % 100;
				if(enemies[i] == NULL)
				{
					type = (enemy_rand < 15 ? 2 : enemy_rand < 65 ? 3 : 1);
					enemies[i] = createEnemyShip(renderer, x, -50, type, shipTextures[type - 1]);
					break;
				}
			}
		}
		futurSpawnEnemy = actualTime + 1 + rand() % (3);;
	}
}

void getLaserCollision()
{
	/* Utilisation d'un double parcours, pour chaque lasers, on verifie si chaque énnemie entre en collision ou non avec. */

	spaceShipLaser* laser = NULL;
	spaceShip* enemy = NULL;
	int l = 0;
	int e = 0;
	int j = 0;

	for(l = 0; l < 1000; l++)
	{
		if(lasers[l] != NULL)
		{
			laser = lasers[l];
			for(e = 0; e < 1000; e++)
			{
				if(enemies[e] != NULL)
				{
					enemy = enemies[e];
					if(laser->rect.x < enemy->rect.x + enemy->rect.w &&
	   					laser->rect.x + laser->rect.w > enemy->rect.x &&
	   					laser->rect.y < enemy->rect.y + enemy->rect.h &&
	   					laser->rect.h + laser->rect.y > enemy->rect.y) /* Vérification d'une collision entre un laser et un ennemi */
					{
						score += enemy->points; /* Ajout des points au score */

						SDL_DestroyTexture(laser->texture);
						free(lasers[l]);
						lasers[l] = NULL;

						/* Création de l'explosion */
						for(j = 0; j < 1000; j++)
						{
							if(explosions[j] == NULL)
							{
								explosions[j] = createExplosion(enemy);
								break;
							}
						}

						enemies[e] = NULL;
						break;
					}
				}
			}
		}
	}
}

void showScore()
{
	SDL_Surface* scoreSurface = NULL;
	TTF_Font* police = NULL;
	SDL_Color color = {255, 255, 255};
	SDL_Rect position;
	char scoreText[10];

	sprintf(scoreText, "%d", score);

	position.x = 0;
    position.y = 0;
	position.h = 40;
	position.w = strlen(scoreText) * 20; /* la largeur de l'affichage dépend du nombre de chiffres du score */

	SDL_DestroyTexture(scoreTexture);

	scoreSurface = TTF_RenderText_Solid(font, scoreText, color);

	scoreTexture = SDL_CreateTextureFromSurface(renderer, scoreSurface);

	SDL_RenderCopy(renderer, scoreTexture, NULL, &position);

	SDL_FreeSurface(scoreSurface);
}
